from lib.flask_mailplus import send_template_message
from watchers.app import create_celery_app

import glob, os, time
from celery.utils.log import get_task_logger
logger = get_task_logger(__name__)

celery = create_celery_app()


@celery.task()
def scan_files():
    """
    Scan directory for files.
    :param: None
    :return: None
    """
    logger.debug("scan_files")
    SCAN_DIR = os.getenv('SCAN_DIR', '.')
    SCAN_FILTER = os.getenv('SCAN_FILTER', '.')
    #dir = '/Users/james/Downloads/**/*.jpg'
    src_dir = os.path.join(os.path(SCAN_DIR), os.path(SCAN_FILTER))
    n, t = 0, time.time()
    n = glob.glob(src_dir, recursive=True)
    t = time.time() - t
    logger.info("DIR: {}".format(src_dir))
    logger.info("COUNT: {}".format(len(n)))
    logger.info("TIME: {}".format(t))
    return None



# THE TASK WHICH CAME FROM https://github.com/nickjj/build-a-saas-app-with-flask/
#
@celery.task()
def deliver_contact_email(email, message):
    """
    Send a contact e-mail.

    :param email: E-mail address of the visitor
    :type user_id: str
    :param message: E-mail message
    :type user_id: str
    :return: None
    """
    ctx = {'email': email, 'message': message}

    send_template_message(subject='[Watchers] Contact',
                          sender=email,
                          recipients=[celery.conf.get('MAIL_USERNAME')],
                          reply_to=email,
                          template='contact/mail/index', ctx=ctx)

    return None
