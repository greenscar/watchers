# from flask import Flask, render_template
#
# application = Flask(__name__)
#
#
# @application.route('/')
# def index():
#     return render_template("index.html")
#
# if __name__ == "__main__":
#     application.run(host="0.0.0.0", port=80)
#
#
#
# https://github.com/nickjj/build-a-saas-app-with-flask/blob/master
from flask import Flask
from celery import Celery
from werkzeug.debug import DebuggedApplication

from watchers.blueprints.page import page
from watchers.blueprints.contact import contact
from watchers.extensions import debug_toolbar, mail, csrf, flask_static_digest

CELERY_TASK_LIST = [
    'watchers.blueprints.contact.tasks',
]


def create_celery_app(app=None):
    """
    Create a new Celery object and tie together the Celery config to the app's
    config. Wrap all tasks in the context of the application.
    :param app: Flask app
    :return: Celery app
    """
    app = app or create_app()

    celery = Celery(app.import_name, broker=app.config['CELERY_BROKER_URL'],
                    include=CELERY_TASK_LIST)
    celery.conf["worker_send_task_events"] = True
    # from pprint import pprint
    # print("-" * 30)
    # pprint(celery.conf)
    # print("-" * 30)

    celery.conf.update(app.config)
    #celery.autodiscover_tasks(lambda: app.config["INSTALLED_APPS"])

    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)

    celery.Task = ContextTask
    return celery


def create_app(settings_override=None):
    """
    Create a Flask application using the app factory pattern.
    :param settings_override: Override settings
    :return: Flask app
    """
    app = Flask(__name__, static_folder='../public', static_url_path='')

    app.config.from_object('config.settings')

    if settings_override:
        app.config.update(settings_override)

    app.register_blueprint(page)
    app.register_blueprint(contact)
    extensions(app)

    if app.debug:
        app.wsgi_app = DebuggedApplication(app.wsgi_app, evalex=True)

    return app


def extensions(app):
    """
    Register 0 or more extensions (mutates the app passed in).
    :param app: Flask application instance
    :return: None
    """
    debug_toolbar.init_app(app)
    mail.init_app(app)
    csrf.init_app(app)
    flask_static_digest.init_app(app)

    return None