FROM alpine:3.7
ENV APP_HOME /usr/src/app
VOLUME $APP_HOME/public
WORKDIR $APP_HOME
RUN apk add --no-cache \
        uwsgi-python3 \
        python3
#
#ENV STATIC_URL /static
#ENV STATIC_PATH /var/www/app/static

# install pipenv
RUN set -ex && pip3 install pipenv --upgrade

# copy pipfiles
COPY Pipfile Pipfile
COPY Pipfile.lock Pipfile.lock

# -- Install Python dependencies:
RUN set -ex && pipenv install --deploy --system

#ENV PYTHONPATH $APP_HOME

ADD watchers $APP_HOME
EXPOSE 8000
WORKDIR $APP_HOME
# https://medium.com/@kmmanoj/deploying-a-scalable-flask-app-using-gunicorn-and-nginx-in-docker-part-1-3344f13c9649
#CMD ["gunicorn", "-b", "0.0.0.0:8000", "app"]
#CMD [ "uwsgi", "--socket", "0.0.0.0:8000", \
#               "--uid", "uwsgi", \
#               "--plugins", "python3", \
#               "--protocol", "uwsgi", \
#               "--wsgi", "wsgi:application" ]



ARG FLASK_ENV="development"
ENV FLASK_ENV="${FLASK_ENV}" \
    FLASK_APP="watchers.app" \
    PYTHONUNBUFFERED="true"

COPY . .
#
#RUN pip install --editable .

RUN if [ "${FLASK_ENV}" != "development" ]; then \
  flask digest compile; fi

CMD ["gunicorn", "-c", "python:config.gunicorn", "watchers.app:create_app()"]